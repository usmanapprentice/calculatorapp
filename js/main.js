window.onload = function () {
	var displayText = document.getElementById('display'),
	Expression='', Operand = ['.', '+', '-', '*', '/'], key;
	let lastOperatorPos = -1;

	function validateInput (key, Operand) {
		for (operator of Operand) {
			if (operator === key) {
				return false;
			}
		}
		return true;
	}

	let buttons = document.getElementsByClassName(' btn-num');
	for (button of buttons) {
		button.addEventListener('click', function(){
			Expression += this.value;
			displayText.value = Expression;
		});
	}

	let operands = document.getElementsByClassName('btn-operand');
	for (operand of operands) {
		operand.addEventListener('click', function(){
			key = Expression.substring(Expression.length-1);
			if (validateInput(key, Operand)) {
				Expression += this.value;
				let sign = this.value;
				displayText.value = Expression;
				lastOperatorPos = Expression.length;
			}
		});
	}

	document.getElementById('decBtn').addEventListener('click', function () {
		key = Expression.substring(Expression.length-1);
		if (validateInput(key, Operand) && Expression.indexOf('.', lastOperatorPos) === -1) {
			Expression += '.';
			displayText.value = Expression;
		}
	});
	document.getElementById('clearBtn').addEventListener('click', function () {
		Expression = '';
		displayText.value = Expression;
	});

	document.getElementById('eqBtn').addEventListener('click', function () {
		let delim = Expression[Expression.length-1];
		if (validateInput(delim, Operand) == false) {
			displayText.value = 'Invalid Expression!!!';
			Expression = ' ';
		} else{
			let result = eval(Expression).toFixed(2);
			displayText.value =Expression +' = ' +result;
			Expression = result.toString();
		}
	});

}
