Calculator Task  
 
1.Project brief Technical details: 
 
This project requires beginner coding skills. The project should follow HTML5 and CSS3 standards. 
Folder structure: 
index.html – contains the HTML code for the calculator 
CSS folder (/css) contains style.css file 
images folder (/images) contains all the images used for this project 
javascript folder (/js) contains all the javascript (jQuery) files used for this project starting with main.js that contains the functions loaded after the DOM is ready 
 
2.Other instructions The calculator should be able to add, subtract, multiply or divide two or more numbers correctly 
The input field must be validated with regex to accept only numeric and mathematical signs 
There should be a clear latest input character and clear all input buttons 
The calculations must work with decimals 
 
3.Useful links for study http://regexr.com/ 
http://www.w3schools.com/jsref/jsref_eval.asp 